\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\nonumberline Abbildungsverzeichnis}{IV}{section*.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}Erkl\IeC {\"a}rung des Themas}{1}{subsection.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}Zielsetzung}{1}{subsection.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3}Aufbau der Arbeit}{1}{subsection.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.4}Abgrenzung}{2}{subsection.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}International Software Testing Qualifications Board}{3}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Entstehung des ISTQB}{3}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Mission und Vision}{3}{subsection.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.1}ISTQB Vision}{3}{subsubsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.2}ISTQB Mission}{3}{subsubsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Zertifizierungen}{4}{subsection.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.1}Foundation Level}{4}{subsubsection.2.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.2}Advanced Level}{4}{subsubsection.2.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.3}Expert Level}{5}{subsubsection.2.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Fundamentaler Testprozess}{6}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Testplanung und Steuerung}{7}{subsection.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1}Testplanung}{7}{subsubsection.3.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.2}Teststeuerung}{7}{subsubsection.3.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Testanalyse und Testentwurf}{7}{subsection.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.1}Testanalyse}{7}{subsubsection.3.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.2}Testentwurf}{8}{subsubsection.3.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}Testrealisierung und Testdurchf\IeC {\"u}hrung}{8}{subsection.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4}Bewertung von Endkriterien und Bericht}{8}{subsection.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5}Abschluss der Testaktivit\IeC {\"a}ten}{9}{subsection.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Teststufen}{10}{section.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Testing in einem Vorgehensmodell}{10}{subsection.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Modultest}{11}{subsection.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3}Integrationstest}{12}{subsection.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4}Systemtest}{12}{subsection.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5}Abnahmetest}{13}{subsection.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.5.1}Anwender-Abnahmetest}{13}{subsubsection.4.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.5.2}Betrieblicher Abnahmetest}{13}{subsubsection.4.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.5.3}Vertraglicher und regulatorischer Abnahmetest}{13}{subsubsection.4.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.5.4}Alpha- und Beta-Test}{14}{subsubsection.4.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Testarten}{15}{section.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}Funktionaler Test}{15}{subsection.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.1.1}Nicht-funktionaler Test}{15}{subsubsection.5.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.1.2}Strukturbasierter Test}{16}{subsubsection.5.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.1.3}Fehlernachtest und Regressionstest}{16}{subsubsection.5.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.1.4}Wartungstest}{17}{subsubsection.5.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Praxis in einem E-Commerce Projekt}{18}{section.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Vorstellung Projektumfang}{18}{subsection.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}Anwendung auf: Fundamentaler Testprozess}{18}{subsection.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.1}Testplanung und Steuerung}{18}{subsubsection.6.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.2}Testanalyse und Testentwurf}{19}{subsubsection.6.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.3}Testrealisierung und Testdurchf\IeC {\"u}hrung}{20}{subsubsection.6.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.4}Bewertung von Endkriterien und Bericht}{21}{subsubsection.6.2.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.2.5}Abschluss der Testaktivit\IeC {\"a}t}{21}{subsubsection.6.2.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.3}Anwendung auf: Teststufen}{21}{subsection.6.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.3.1}Modultest}{21}{subsubsection.6.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.3.2}Integrationstest}{21}{subsubsection.6.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.3.3}Systemtest}{22}{subsubsection.6.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.3.4}Abnahmetest}{22}{subsubsection.6.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.4}Anwendung auf: Testarten}{22}{subsection.6.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.4.1}Funktionale Tests}{22}{subsubsection.6.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.4.2}Nicht-Funktionale Tests}{22}{subsubsection.6.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.4.3}Strukturbezogener Test}{22}{subsubsection.6.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.4.4}Regressionstest und Fehlernachtest}{23}{subsubsection.6.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {6.4.5}Wartungstest}{23}{subsubsection.6.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}Schlussbetrachtung}{24}{section.7}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1}Fazit}{24}{subsection.7.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2}Ausblick}{24}{subsection.7.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\nonumberline Literaturverzeichnis}{IV}{section*.11}
\contentsfinish 
