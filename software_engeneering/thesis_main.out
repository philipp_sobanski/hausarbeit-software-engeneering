\BOOKMARK [1][-]{section*.2}{Abbildungsverzeichnis}{}% 1
\BOOKMARK [1][-]{section.1}{Einleitung}{}% 2
\BOOKMARK [2][-]{subsection.1.1}{Erkl\344rung des Themas}{section.1}% 3
\BOOKMARK [2][-]{subsection.1.2}{Zielsetzung}{section.1}% 4
\BOOKMARK [2][-]{subsection.1.3}{Aufbau der Arbeit}{section.1}% 5
\BOOKMARK [2][-]{subsection.1.4}{Abgrenzung}{section.1}% 6
\BOOKMARK [1][-]{section.2}{International Software Testing Qualifications Board}{}% 7
\BOOKMARK [2][-]{subsection.2.1}{Entstehung des ISTQB}{section.2}% 8
\BOOKMARK [2][-]{subsection.2.2}{Mission und Vision}{section.2}% 9
\BOOKMARK [3][-]{subsubsection.2.2.1}{ISTQB Vision}{subsection.2.2}% 10
\BOOKMARK [3][-]{subsubsection.2.2.2}{ISTQB Mission}{subsection.2.2}% 11
\BOOKMARK [2][-]{subsection.2.3}{Zertifizierungen}{section.2}% 12
\BOOKMARK [3][-]{subsubsection.2.3.1}{Foundation Level}{subsection.2.3}% 13
\BOOKMARK [3][-]{subsubsection.2.3.2}{Advanced Level}{subsection.2.3}% 14
\BOOKMARK [3][-]{subsubsection.2.3.3}{Expert Level}{subsection.2.3}% 15
\BOOKMARK [1][-]{section.3}{Fundamentaler Testprozess}{}% 16
\BOOKMARK [2][-]{subsection.3.1}{Testplanung und Steuerung}{section.3}% 17
\BOOKMARK [3][-]{subsubsection.3.1.1}{Testplanung}{subsection.3.1}% 18
\BOOKMARK [3][-]{subsubsection.3.1.2}{Teststeuerung}{subsection.3.1}% 19
\BOOKMARK [2][-]{subsection.3.2}{Testanalyse und Testentwurf}{section.3}% 20
\BOOKMARK [3][-]{subsubsection.3.2.1}{Testanalyse}{subsection.3.2}% 21
\BOOKMARK [3][-]{subsubsection.3.2.2}{Testentwurf}{subsection.3.2}% 22
\BOOKMARK [2][-]{subsection.3.3}{Testrealisierung und Testdurchf\374hrung}{section.3}% 23
\BOOKMARK [2][-]{subsection.3.4}{Bewertung von Endkriterien und Bericht}{section.3}% 24
\BOOKMARK [2][-]{subsection.3.5}{Abschluss der Testaktivit\344ten}{section.3}% 25
\BOOKMARK [1][-]{section.4}{Teststufen}{}% 26
\BOOKMARK [2][-]{subsection.4.1}{Testing in einem Vorgehensmodell}{section.4}% 27
\BOOKMARK [2][-]{subsection.4.2}{Modultest}{section.4}% 28
\BOOKMARK [2][-]{subsection.4.3}{Integrationstest}{section.4}% 29
\BOOKMARK [2][-]{subsection.4.4}{Systemtest}{section.4}% 30
\BOOKMARK [2][-]{subsection.4.5}{Abnahmetest}{section.4}% 31
\BOOKMARK [3][-]{subsubsection.4.5.1}{Anwender-Abnahmetest}{subsection.4.5}% 32
\BOOKMARK [3][-]{subsubsection.4.5.2}{Betrieblicher Abnahmetest}{subsection.4.5}% 33
\BOOKMARK [3][-]{subsubsection.4.5.3}{Vertraglicher und regulatorischer Abnahmetest}{subsection.4.5}% 34
\BOOKMARK [3][-]{subsubsection.4.5.4}{Alpha- und Beta-Test}{subsection.4.5}% 35
\BOOKMARK [1][-]{section.5}{Testarten}{}% 36
\BOOKMARK [2][-]{subsection.5.1}{Funktionaler Test}{section.5}% 37
\BOOKMARK [3][-]{subsubsection.5.1.1}{Nicht-funktionaler Test}{subsection.5.1}% 38
\BOOKMARK [3][-]{subsubsection.5.1.2}{Strukturbasierter Test}{subsection.5.1}% 39
\BOOKMARK [3][-]{subsubsection.5.1.3}{Fehlernachtest und Regressionstest}{subsection.5.1}% 40
\BOOKMARK [3][-]{subsubsection.5.1.4}{Wartungstest}{subsection.5.1}% 41
\BOOKMARK [1][-]{section.6}{Praxis in einem E-Commerce Projekt}{}% 42
\BOOKMARK [2][-]{subsection.6.1}{Vorstellung Projektumfang}{section.6}% 43
\BOOKMARK [2][-]{subsection.6.2}{Anwendung auf: Fundamentaler Testprozess}{section.6}% 44
\BOOKMARK [3][-]{subsubsection.6.2.1}{Testplanung und Steuerung}{subsection.6.2}% 45
\BOOKMARK [3][-]{subsubsection.6.2.2}{Testanalyse und Testentwurf}{subsection.6.2}% 46
\BOOKMARK [3][-]{subsubsection.6.2.3}{Testrealisierung und Testdurchf\374hrung}{subsection.6.2}% 47
\BOOKMARK [3][-]{subsubsection.6.2.4}{Bewertung von Endkriterien und Bericht}{subsection.6.2}% 48
\BOOKMARK [3][-]{subsubsection.6.2.5}{Abschluss der Testaktivit\344t}{subsection.6.2}% 49
\BOOKMARK [2][-]{subsection.6.3}{Anwendung auf: Teststufen}{section.6}% 50
\BOOKMARK [3][-]{subsubsection.6.3.1}{Modultest}{subsection.6.3}% 51
\BOOKMARK [3][-]{subsubsection.6.3.2}{Integrationstest}{subsection.6.3}% 52
\BOOKMARK [3][-]{subsubsection.6.3.3}{Systemtest}{subsection.6.3}% 53
\BOOKMARK [3][-]{subsubsection.6.3.4}{Abnahmetest}{subsection.6.3}% 54
\BOOKMARK [2][-]{subsection.6.4}{Anwendung auf: Testarten}{section.6}% 55
\BOOKMARK [3][-]{subsubsection.6.4.1}{Funktionale Tests}{subsection.6.4}% 56
\BOOKMARK [3][-]{subsubsection.6.4.2}{Nicht-Funktionale Tests}{subsection.6.4}% 57
\BOOKMARK [3][-]{subsubsection.6.4.3}{Strukturbezogener Test}{subsection.6.4}% 58
\BOOKMARK [3][-]{subsubsection.6.4.4}{Regressionstest und Fehlernachtest}{subsection.6.4}% 59
\BOOKMARK [3][-]{subsubsection.6.4.5}{Wartungstest}{subsection.6.4}% 60
\BOOKMARK [1][-]{section.7}{Schlussbetrachtung}{}% 61
\BOOKMARK [2][-]{subsection.7.1}{Fazit}{section.7}% 62
\BOOKMARK [2][-]{subsection.7.2}{Ausblick}{section.7}% 63
\BOOKMARK [1][-]{section*.11}{Literaturverzeichnis}{}% 64
