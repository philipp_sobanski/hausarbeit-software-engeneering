\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {ngerman}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\nonumberline Abbildungsverzeichnis}{IV}{section*.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}Einleitung}{1}{section.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.1}Erkl\IeC {\"a}rung des Themas}{1}{subsection.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.2}Zielsetzung}{1}{subsection.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.3}Aufbau der Arbeit}{1}{subsection.1.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {1.4}Abgrenzung}{1}{subsection.1.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}International Software Testing Qualifications Board}{3}{section.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}Entstehung des ISTQB}{3}{subsection.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}Mission und Vision}{3}{subsection.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.1}ISTQB Vision}{3}{subsubsection.2.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.2.2}ISTQB Mission}{3}{subsubsection.2.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.3}Zertifizierungen}{4}{subsection.2.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.1}Foundation Level}{4}{subsubsection.2.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.2}Advanced Level}{4}{subsubsection.2.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {2.3.3}Expert Level}{5}{subsubsection.2.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}Fundamentaler Testprozess}{6}{section.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.1}Testplanung und Steuerung}{6}{subsection.3.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.1}Testplanung}{6}{subsubsection.3.1.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.1.2}Teststeuerung}{7}{subsubsection.3.1.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.2}Testanalyse und Testentwurf}{7}{subsection.3.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.1}Testanalyse}{7}{subsubsection.3.2.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {3.2.2}Testentwurf}{7}{subsubsection.3.2.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.3}Testrealisierung und Testdurchf\IeC {\"u}hrung}{8}{subsection.3.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.4}Bewertung von Endkriterien und Bericht}{8}{subsection.3.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {3.5}Abschluss der Testaktivit\IeC {\"a}ten}{8}{subsection.3.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}Teststufen}{9}{section.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.1}Testing in einem Vorgehensmodell}{9}{subsection.4.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.2}Modultest}{10}{subsection.4.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.3}Integrationstest}{11}{subsection.4.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.4}Systemtest}{11}{subsection.4.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {4.5}Abnahmetest}{11}{subsection.4.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.5.1}Anwender-Abnahmetest}{12}{subsubsection.4.5.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.5.2}Betrieblicher Abnahmetest}{12}{subsubsection.4.5.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.5.3}Vertraglicher und regulatorischer Abnahmetest}{12}{subsubsection.4.5.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {4.5.4}Alpha- und Beta-Test}{12}{subsubsection.4.5.4}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}Testarten}{13}{section.5}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.0.1}Funktionaler Test}{13}{subsubsection.5.0.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.0.2}Nicht-funktionaler Test}{13}{subsubsection.5.0.2}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.0.3}Strukturbasierter Test}{14}{subsubsection.5.0.3}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.0.4}Fehlernachtest und Regressionstest}{14}{subsubsection.5.0.4}
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.0.5}Wartungstest}{14}{subsubsection.5.0.5}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}Schlussbetrachtung}{16}{section.6}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.1}Fazit}{16}{subsection.6.1}
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {6.2}Ausblick}{16}{subsection.6.2}
\defcounter {refsection}{0}\relax 
\contentsline {section}{\nonumberline Literaturverzeichnis}{IV}{section*.6}
\contentsfinish 
